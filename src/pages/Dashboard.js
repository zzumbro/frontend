import { Col, Container, Row } from 'react-bootstrap'
import { NavHeader } from '../components/NavHeader'
import { UserInformation } from '../components/UserInformation'
import { Earnings } from '../components/Earnings'
import { DashboardCalendar } from '../components/DashboardCalendar'
import { useEffect, useState } from 'react'
import api from '../util/api'

export const Dashboard = () => {
  const [calendarData, setCalendarData] = useState([])
  useEffect(() => {
    api.calendarData().then((resp) => {
      setCalendarData(resp.data)
    })
  }, [])
  return (
    <>
      <NavHeader />
      <Container fluid>
        <Row s>
          <Col s>
            <Earnings user='default' />
          </Col>
          <Col>
            <DashboardCalendar data={calendarData} />
          </Col>
          <Col>
            <UserInformation />
          </Col>
        </Row>
      </Container>
    </>
  )
}
