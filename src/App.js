import './App.css'
import { Dashboard } from './pages/Dashboard'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { useState } from 'react'
import { Button } from 'react-bootstrap'

const history = createBrowserHistory()
export const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState({ isLoggedIn: false, username: null })

  const onLogin = () => {
    history.push('/')
    setIsLoggedIn({ ...isLoggedIn, isLoggedIn: true })
    window.location.reload()
  }

  return (
    <Router history={history}>
      <Switch>
        <Route path='/login' exact>
          <div>
            <h1>Login goes here</h1>
            <Button onClick={onLogin}>Login </Button>
          </div>
        </Route>
        <Route path='/logout' exact>
          <div>
            <h3>Hello you should be logged out</h3>
          </div>
        </Route>
        <PrivateRoute isLoggedIn={isLoggedIn} path='/' />
      </Switch>
    </Router>
  )
}

const PrivateRoute = ({ isLoggedIn, children, ...rest }) => (
  <div className='container-fluid'>
    <Route {...rest} render={() => (isLoggedIn ? <Dashboard /> : <Redirect to='/login' />)} />
  </div>
)
