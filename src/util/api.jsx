import axios from 'axios'

const api_from_window = () => {
  const url = window.location.href
  const slash_loc = url.indexOf('/', 8)
  return url.substr(0, slash_loc + 1) + 'api'
}

const baseUrl = process.env.REACT_APP_API_URL || api_from_window()

const post = (endpoint, data, query_params) => {
  const url = `${baseUrl}${endpoint}`
  return axios.post(url, data, {
    params: query_params,
  })
}

const api = {
  isLoggedIn: () => true,
  authenticate: () => true,
  logout: () => false,
  calendarData: () => post('/calendar'),
  earnings: () => post('/earnings'),
  search: (data) => post('/search', data),
}

export default api
