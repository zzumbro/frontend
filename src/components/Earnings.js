import { Card, Spinner } from 'react-bootstrap'
import { useEffect, useState } from 'react'
import api from '../util/api'

export const Earnings = () => {
  const [earnings, setEarnings] = useState({})
  const total = earnings.total !== null

  useEffect(() => {
    api.earnings().then(({ data }) => {
      setEarnings(data)
    })
  }, [total])

  if (!total) return <Spinner animation='grow' />
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Title>Earnings</Card.Title>
        <Card.Subtitle>{earnings.total}</Card.Subtitle>
      </Card.Body>
    </Card>
  )
}
