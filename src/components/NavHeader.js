import { Button, Form, FormControl, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import api from '../util/api'

export const NavHeader = () => {
  const sendData = (e) => {
    const formData = new FormData(e.target)
    const formDataObj = Object.fromEntries(formData.entries())
    api.search(formDataObj).then((resp) => {
      console.log(resp)
    })
  }
  return (
    <Navbar bg='light' expand='lg'>
      <Navbar.Brand href='#home'>React-Bootstrap</Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='mr-auto'>
          <Nav.Link href='#home'>Home</Nav.Link>
          <Nav.Link href='#link'>Link</Nav.Link>
          <NavDropdown title='Dropdown' id='basic-nav-dropdown'>
            <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
            <NavDropdown.Item href='#action/3.2'>Another action</NavDropdown.Item>
            <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href='#action/3.4'>Separated link</NavDropdown.Item>
          </NavDropdown>
        </Nav>
        <Form className='search-form' onSubmit={sendData} inline>
          <FormControl type='text' placeholder='Search' className='mr-sm-2' name='search' />
          <Button variant='outline-success' type='submit'>
            Search
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  )
}
