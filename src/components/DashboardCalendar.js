import { ResponsiveCalendar } from '@nivo/calendar'
import { Container } from 'react-bootstrap'
export const DashboardCalendar = ({ data }) => {
  console.log(data)
  return (
    <Container style={{ height: '300px', width: '600px' }}>
      <ResponsiveCalendar
        data={data}
        from='2019-03-01'
        to='2019-07-12'
        emptyColor='#aa7942'
        colors={['#61cdbb', '#97e3d5', '#e8c1a0', '#f47560']}
        margin={{ top: 40, right: 40, bottom: 50, left: 40 }}
        direction='horizontal'
        monthBorderColor='#ffffff'
        dayBorderWidth={0}
        dayBorderColor='#ffffff'
        legends={[
          {
            anchor: 'bottom-right',
            direction: 'row',
            translateY: 36,
            itemCount: 4,
            itemWidth: 42,
            itemHeight: 36,
            itemsSpacing: 14,
            itemDirection: 'right-to-left',
          },
        ]}
      />
    </Container>
  )
}
