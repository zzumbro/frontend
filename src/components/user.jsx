import { useEffect, useState } from 'react'
import axios from 'axios'
export const User = () => {
  const [data, setData] = useState({ name: 'default' })
  useEffect(function foo() {
    axios.post('http://localhost:8000/get_data').then((res) => {
      setData(res.data)
    })
    axios.post('http://localhost:8000/data', { name: 'professor' })
  }, [])
  return (
    <div>
      information<div>Name: {data.name}</div>
    </div>
  )
}

// function User(name) {
//   return (
//     <div>
//       information<div>Name: {name}</div>
//     </div>
//   )
// }
